package com.example.carrental.repository;

import com.example.carrental.classes.Message.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}


