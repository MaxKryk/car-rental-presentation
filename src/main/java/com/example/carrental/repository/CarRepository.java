package com.example.carrental.repository;

import com.example.carrental.classes.Car.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Long> {

    List<Car> findByBrand(String brand);
    List<Car> findByName(String name);
/*
OLD METHODS FROM USER:
    Optional<User> findById(Long id);
    List<User> findByFirstName(String firstName);
    List <User> findByLastName(String lastName);
    List <User> findByLogin(String login);
    List <User> findByMail(String mail);*/
}
