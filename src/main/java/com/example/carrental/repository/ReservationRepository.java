package com.example.carrental.repository;

import com.example.carrental.classes.Mail.EmailService;
import com.example.carrental.classes.Message.Message;
import com.example.carrental.classes.Reservation.Reservation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;



public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}


