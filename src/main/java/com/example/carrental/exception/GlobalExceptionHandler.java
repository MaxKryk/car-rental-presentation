package com.example.carrental.exception;



import com.example.carrental.classes.InfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
        import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.example.carrental.controller.ErrorController;
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {



    //StandardServletMultipartResolver
    @ExceptionHandler(MultipartException.class)
    public String handleError1(MultipartException e, RedirectAttributes redirectAttributes) {
        log.info( "jeblo" );//TODO
        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:/uploadStatus";

    }

    //CommonsMultipartResolver
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes) {
        log.info( "jeblo2" );//TODO do widoku
        redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
        return "redirect:/uploadStatus";

    }
    @ExceptionHandler(CarNotFoundException.class)
    public ModelAndView handleCarNotFoundException(CarNotFoundException e, Model model){
        log.info( "exception handler" );
        ErrorController errorController=new ErrorController();
        log.info( "@GET error_info" );
        InfoDTO infoDTO =new InfoDTO();
        model.addAttribute( "infoDto", infoDTO );
        infoDTO.setMessage(e.getMessage());



    return errorController.getError(model);


    }




}
