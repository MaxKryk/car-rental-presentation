package com.example.carrental.controller;

import com.example.carrental.classes.Car.Car;
import com.example.carrental.classes.Message.Message;
import com.example.carrental.classes.Reservation.Reservation;

import com.example.carrental.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Slf4j
@RestController

public class WebController {
    private final CarRepository carRepository;
    WebController(CarRepository carRepository){this.carRepository=carRepository;}

    org.slf4j.Logger webControllerLogger = LoggerFactory.getLogger("Web controller");


    @GetMapping("/")
    public ModelAndView getMainPage(Model model) {
        webControllerLogger.info("redirects to /index");
        return new ModelAndView("redirect:/index");
    }


//    @GetMapping("/login")
//    public ModelAndView getAdminLoginPage(Model model) {
//        webControllerLogger.info("redirects to /admin_login_page");
//        return new ModelAndView("/login");
//    }

    @GetMapping("/index")
    public ModelAndView findAll(Model model) {
        List<Car> allCars = carRepository.findAll();
        model.addAttribute( "reservation", new Reservation ());
//model.addAttribute( "countRentalCostDTO", new CountRentalCostDTO() );
//        for (Car car:allCars)
//            car.fillDataToDisplay();
        model.addAttribute( "cars", allCars );
        model.addAttribute( "message", new Message() );
        return new ModelAndView( "index" );
    }

//todo delete
    @GetMapping("/info")
    public ModelAndView getInfo(Model model) {
        return new ModelAndView( "info" );
    }
//todo delete
@GetMapping("/send_mail1")
public ModelAndView sendMail(Model model) {
    return new ModelAndView( "send_mail1" );
}
}