package com.example.carrental.controller;


import com.example.carrental.classes.InfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collection;
import java.util.Map;

@Controller
@RestController
@Slf4j
public class ErrorController {

    @GetMapping("/error_info")
    public ModelAndView getError(Model model){
        return new ModelAndView( "error_admin" );
    }}