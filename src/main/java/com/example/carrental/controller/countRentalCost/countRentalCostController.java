package com.example.carrental.controller.countRentalCost;

import com.example.carrental.classes.Car.Car;
import com.example.carrental.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Slf4j
@RestController
public class countRentalCostController {


        @Autowired
        private final CarRepository carRepository;

        public countRentalCostController(CarRepository carRepository) {
            this.carRepository = carRepository;
        }

        @PostMapping("/count_rental_cost")
        public String countRentalCost(Model model, @ModelAttribute CountRentalCostDTO countRentalCostDTO){
            log.info( "@PostMapping(\"/count_rental_cost\")" );
long id = carRepository.findByName(countRentalCostDTO.getCarName()).get( 0 ).getId();
Optional<Car> car=carRepository.findById( id  );
log.info( "car.toString(): "+car.toString());
//output: tytuł przelewu, nazwa auta, koszt
return new String(  countRentalCostDTO.toString()+"id: " + id);
}





    }

