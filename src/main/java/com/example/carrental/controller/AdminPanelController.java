package com.example.carrental.controller;

import com.example.carrental.classes.Car.Car;
import com.example.carrental.classes.Car.FindCarDto;
import com.example.carrental.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Slf4j
@RestController
public class AdminPanelController {
    private final CarRepository carRepository;
    AdminPanelController(CarRepository carRepository){this.carRepository=carRepository;}
    @GetMapping("/admin_panel")
    public ModelAndView getAdminIndex(Model model) {
        List<Car> allCars = carRepository.findAll();

        model.addAttribute( "car", new Car() );
        model.addAttribute( "findCarDto", new FindCarDto() );

//        for (Car car:allCars) {
//            car.fillDataToDisplay();
//            log.info( "car data" );
//            log.info( car.getData() );
//        }
        model.addAttribute( "cars", allCars );

        return new ModelAndView( "admin_panel" );
    }
}
