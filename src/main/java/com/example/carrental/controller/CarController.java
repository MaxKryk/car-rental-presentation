package com.example.carrental.controller;

import com.example.carrental.File.FileService;
import com.example.carrental.classes.Car.*;
import com.example.carrental.classes.InfoDTO;
import com.example.carrental.exception.CarNotFoundException;
import com.example.carrental.exception.WrongInputException;
import com.example.carrental.repository.CarRepository;
import com.example.carrental.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@RestController
@SessionAttributes({"ident", "foundCar"})
public class CarController {
        @Autowired
    private final CarRepository carRepository;
    private final FileService fileService;



        private final MessageRepository messageRepository;
        CarController(CarRepository carRepository, FileService fileService, MessageRepository messageRepository) {
            this.carRepository = carRepository;
            this.fileService = fileService;
                        this.messageRepository = messageRepository;}



    Logger logger = Logger.getLogger( "car controller logger: " );
//        @GetMapping("/cars3")
//    public ModelAndView findAll(Model model){
//        List<Car> allCars3 = carRepository.findAll();
//        model.addAttribute( "cars3", allCars3 );
//        return new ModelAndView( "admin_all_cars3" );
//    }
//    @GetMapping("/cars")
//    public ModelAndView findAll(Model model){
//        List<Car> allCars = carRepository.findAll();
//        model.addAttribute( "cars", allCars );
//        return new ModelAndView( "admin_all_cars" );
//    }
    @GetMapping("/cars:{id}")
    public ModelAndView showOne(@PathVariable Long id, Model model){

        logger.info( "ID: " + id );
        //founcar
        Car foundCar = carRepository.findById(id).orElseThrow( ()->new CarNotFoundException( id ) );
        model.addAttribute( "foundCar", foundCar );
        //template to update
        CarTemplateToUpdateDTO carTemplateToUpdateDTO=new CarTemplateToUpdateDTO();
        model.addAttribute( "newCarTemplateToUpdateDTO", carTemplateToUpdateDTO);
//ident
        Ident ident = new Ident();
        ident.setId( id );
        model.addAttribute( "ident", ident );
        logger.info( "@GetMapping(\"/cars:{id}\") logic ends");

        InfoDTO infoDto=new InfoDTO();
        infoDto.setMessage( "AUTO O ID " + id + " ZOSTAŁO ODNALEZIONE!" );
        model.addAttribute( "infoDTO", infoDto );

        return new ModelAndView( "car_info" );
    }




//    @GetMapping("/add_new_car")
//    public ModelAndView addCar(Model model){
//        model.addAttribute( "car", new Car() );
//
//        return new ModelAndView( "add_new_car" );
//    }
//
//    @GetMapping("/add_new_car_with_image")
//    public ModelAndView addCar3(Model model){
//        model.addAttribute( "car", new Car() );
//
//////TODO FILE UPLOAD
//        return new ModelAndView( "add_new_car3" );
//    }
    @PostMapping("/add_new_car")
    public ModelAndView postCar(Model model, @ModelAttribute Car car, @RequestParam("file") MultipartFile multipartFile)  {
            List<Car> cars=carRepository.findByName( car.getName() );
            if (cars.size()!=0){
                log.info( "err123: cars.size()!=0" );
                InfoDTO infoDto = new InfoDTO();
                infoDto.setMessage( "Auto o tej nazwie już istnieje w bazie danych" );
                model.addAttribute( "infoDto", infoDto);
                return new ModelAndView( "error_admin" );
            }
//file upload
        fileService.uploadFile( multipartFile );
        log.info("f uploaded");
        File file = new File("\\car-rental\\src\\main\\resources\\static\\img", multipartFile.getOriginalFilename());
        log.info( "name: "  + file.getName() + "and path: " +file.getPath() );
        System.out.println("bef path" + car.toString());
        car.setImageLink( file.getName() );
        car.createPath();
        System.out.println("after path" + car.toString());
//create car object and save it to repository
        carRepository.save( car );
//log.info("carAndFileDTO.toString() " + carAndFileDTO.toString());
        InfoDTO infoDto=new InfoDTO();
        infoDto.setMessage( "AUTO ZOSTAŁO DODANE DO BAZY!" );


        model.addAttribute( "infoDTO", infoDto );
        return new ModelAndView( "add_new_car_success" );


    }


    @PostMapping("/find_car")
    public ModelAndView submitParameter(@ModelAttribute FindCarDto findCarDto, Model model) {
        logger.info( "car post mapping begins" );
        List<Car> foundCars = new ArrayList<>();
String ModelAndViewToReturn="";
        logger.info( findCarDto.toString() );
        switch (findCarDto.getSearchBy()) {
            case "id":
                logger.info( "Searching by id...");
                Long idToFind=new Long(0);
                try {
                    idToFind = new Long( findCarDto.getSearchCriterion() );

                }catch(Exception e){
                    throw new WrongInputException();

                }

                Long finalIdToFind = idToFind;
                logger.info( "Searching by id... ID: " + idToFind);
                foundCars.add(
                        carRepository.findById( idToFind ).orElseThrow( () -> new CarNotFoundException( finalIdToFind ) ));

                logger.info( "@PostMapping(\"/find_user\") something went wrong" );

                break;
            case "brand":
                logger.info( "Searching by brand" );
                foundCars = carRepository.findByBrand( findCarDto.getSearchCriterion() );
                log.info( "list of found cars with size: " + foundCars.size() );
                for (Car element : foundCars) {
                    logger.info( element.toString() );
                }
                break;


                }
                Car foundCar = new Car();
        Ident ident=new Ident(  );
        InfoDTO infoDto=new InfoDTO();
        model.addAttribute( "infoDTO", infoDto );



        if (foundCars.size() == 1) {
                    foundCar = foundCars.get( 0 );
            model.addAttribute( "foundCar", foundCar );
            logger.info( "idfnd " + foundCar.getId() );
ident.setId( foundCar.getId() );
            model.addAttribute( "ident", ident );
                    logger.info( "one car fnd" );
                    logger.info( foundCar.toString() );
                    infoDto.setMessage( "ZNALEZIONO JEDNO AUTO" );
foundCars.clear();
ModelAndViewToReturn="car_info";

                }else if(foundCars.size() >= 1){
model.addAttribute( "foundCars", foundCars );
ModelAndViewToReturn="many_cars_found";
                }
                else if(foundCars.size() == 0){
            log.info( "NO CARS FNDDD" );
                ident.setId( foundCar.getId());
                infoDto.setMessage( "NIE ZNALEZIONO AUTA O PODANEJ MARCE" );
                model.addAttribute( "infoDTO", infoDto );
            ModelAndViewToReturn="info_admin";
        }

log.info("infoDto.toString();");
infoDto.toString();
        return new ModelAndView( ModelAndViewToReturn );

    }

    @PostMapping("/delete_car_info")
    public ModelAndView PostDeleteCarInfo(@ModelAttribute Ident ident, Model model) {
        logger.info( "POST /delete_car_info passed ident: " + ident.getId() );
        carRepository.deleteById( ident.getId() );
        logger.info( "POST /delete_car_info Car with the id : " + ident.getId() + " has been deleted" );

        InfoDTO infoDto=new InfoDTO();
        infoDto.setMessage( "AUTO O ID " + ident.getId() + " ZOSTAŁO USUNIĘTE Z BAZY!" );
        model.addAttribute( "infoDTO", infoDto );

        return new ModelAndView( "info_admin" );
    }


    @PostMapping("/update_car_info")
    public ModelAndView PostUpdateCarInfo(@ModelAttribute Ident ident, Model model, CarTemplateToUpdateDTO carTemplateToUpdateDTO)
    {
        logger.info( "@PostMapping(update_car_info): passed ident: " + ident.getId() );
        logger.info( "@PostMapping(update_car_info): carTemplateToUpdateDTO passed as object: " + carTemplateToUpdateDTO.toString() );
        Car foundCar=carRepository.findById( ident.getId()).orElseThrow( ()-> new CarNotFoundException( ident.getId() ) );
        logger.info( "@PostMapping(update_user_info): unchanged entry: " + foundCar.toString());

        //TWORZENIE OBIEKTU HISTORYCZBEGO
        Car carBeforeUpdate=new Car();
        carBeforeUpdate.setBrand( foundCar.getBrand() );


        foundCar.setBrand( carTemplateToUpdateDTO.getBrand() );
        logger.info( "@PostMapping(update_user_info): changed entry: " + foundCar.toString());
        //#TODO Entry is not changed

        carRepository.save( foundCar );
        System.out.println("casad" + carTemplateToUpdateDTO.toString());
        model.addAttribute( "carBeforeUpdate", carBeforeUpdate );
        model.addAttribute( "carAfterUpdate", foundCar );
        return new ModelAndView( "car_has_been_updated" );
    }
}
