package com.example.carrental.classes.Car;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;

@Data
@Entity
@NoArgsConstructor
public class Car {
    //TODO napisać metodę tworzącą link do kalendarza google
    @Id
    @GeneratedValue long id;
    private String name;
    private String brand;
    private String model;
    private final String path = "img/";
    private String imageLink;

    private String specification;
    private double priceForDay;
    private double priceForWeekend;
    private double priceForWeek;
    private double priceForMonth;
    private String data;

    public Car(String name, String brand, String model, String imageLink, String specification, double priceForDay, double priceForWeekend, double priceForWeek, String data) {
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.imageLink = path+imageLink;
        this.specification = specification;
        this.priceForDay = priceForDay;
        this.priceForWeekend = priceForWeekend;
        this.priceForWeek = priceForWeek;
        this.data = data;
    }

    public Car(String name, String brand, String model, String imageLink, String specification, double priceForDay) {
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.imageLink = path+imageLink;
        this.specification = specification;
        this.priceForDay = priceForDay;
    }

    public Car(String name, String brand, String model, String imageLink, String specification, double v, double v1, double v2) {
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.imageLink = path+imageLink;
        this.specification = specification;
        this.priceForDay = v;
        this.priceForWeekend=v1;
                this.priceForWeek=v2;
    }


    //CONSTRUCTOR
    public void createPath(){
        this.imageLink="img/"+imageLink;
    }

    public Car(String name, String brand, String model, String imageLink, String specification, double priceForDay, double priceForWeekend, double priceForWeek, double priceForMonth, String data) {
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.imageLink = path+imageLink;
        this.specification = specification;
        this.priceForDay = priceForDay;
        this.priceForWeekend = priceForWeekend;
        this.priceForWeek = priceForWeek;
        this.priceForMonth = priceForMonth;
        this.data = data;
    }


    public String toMail() {
        return "Car{" +
                "id=" + id +
                ", nazwa='" + name + '\'' +
                ", marka='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", specyfikacja='" + specification + '\'' +
                ", cena za dzień=" + priceForDay +
                ", cena za weekend='" + priceForWeekend + '\'' +
                ", cena za tydzień='" + priceForWeek + '\'' +
                ", cena za miesiąc='" + priceForMonth + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
