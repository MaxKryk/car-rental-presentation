package com.example.carrental.classes.Car;

import lombok.Data;

@Data
public class FindCarDto {

    private String searchCriterion;
    private String searchBy;
    private long id;
    private String deleteOrUpdate;
    public FindCarDto() {

    }



}
