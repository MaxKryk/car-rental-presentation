package com.example.carrental.classes.Mail;

import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;

public class EmailConfig
{
    @Bean
    public SimpleMailMessage emailTemplate()
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("yourdreamclub809@gmail.com");
        message.setFrom("yourdreamclub809@gmail.com");
        message.setSubject("Important email");
        message.setText("FATAL - Application crash. Save your job !!");
        return message;
    }
}