package com.example.carrental.File;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

        @Slf4j
@Service
    public class FileService {

        //@Value("${app.upload.dir:${user.home}}")
        //@Value("${app.upload.dir:/resources/${java.version}")

        @Value("..\\car-rental\\src\\main\\resources\\static\\img")
                // @Value("C:\\Users\\makryk\\IdeaProjects\\car-rental")

            public String uploadDir;

        public void uploadFile(MultipartFile file) {

            try {
                Path copyLocation = Paths
                        .get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
                Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
           log.info( "copyLocation.toString()" + copyLocation.toString() );
            } catch (Exception e) {
                e.printStackTrace();

                //throw new FileStorageException("Could not store file " + file.getOriginalFilename()
//                        + ". Please try again!");
            }
        }
    }
