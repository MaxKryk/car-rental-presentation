package com.example.carrental;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;



@SpringBootApplication//(exclude = {SecurityAutoConfiguration.class})
@EnableWebMvc
@PropertySource( "classpath:application.properties" )
public class CarRentalApplication extends WebMvcConfigurerAdapter {
    private static final Logger log=LoggerFactory.getLogger( CarRentalApplication.class );

    public static void main(String[] args) {
          SpringApplication.run( CarRentalApplication.class, args );
    }
@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler(
                "/css/**")
                .addResourceLocations( "classpath:/static/css/" );
    registry.addResourceHandler(
            "/img/**")
            .addResourceLocations( "classpath:/static/img/" );
    registry.addResourceHandler(
            "/vendor/**")
            .addResourceLocations( "classpath:/static/vendor/" );
    registry.addResourceHandler(
            "/js/**")
            .addResourceLocations( "classpath:/static/js/" );
    registry.addResourceHandler(
            "/attachments/**")
            .addResourceLocations( "classpath:/attachments/" );
    }



    }


